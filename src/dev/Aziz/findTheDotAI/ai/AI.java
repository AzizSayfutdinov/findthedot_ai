package dev.Aziz.findTheDotAI.ai;

import dev.Aziz.findTheDotAI.display.Display;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class AI implements Runnable{

    private Display display;
    private World world;

    private BufferStrategy bs;
    private Graphics g;

    private boolean running = false;

    private Thread aiThread;

    public AI(){
        display = new Display("Artificial Intelligence", 900, 600);
        world = new World();
        aiThread = new Thread(this);
    }

    @Override
    public void run() {

        int fps = 60;   // frames per second
        double timePerTick = 1000000000 / fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        int ticks = 0;

        while(running){

            now = System.nanoTime();
            delta = delta + ((now - lastTime) / timePerTick);
            timer += now - lastTime;
            lastTime = now;

            if(delta >= 1) {
                update();
                render();
                ticks++;
                delta--;    /// delta = 0;
            }

            if(timer >= 1000000000){
                ticks = 0;
                timer = 0;
            }
        }

        stop();

    }

    public void update(){

        world.update();

    }

    public void render(){
        bs = display.getCanvas().getBufferStrategy();

        if(bs == null){
            display.getCanvas().createBufferStrategy(3);
            return;
        }

        g = bs.getDrawGraphics();
        g.clearRect(0,0,display.getWidth(), display.getHeight());

        if(world != null){
            world.render(g);
        }

        bs.show();
        g.dispose();

    }

    public synchronized void start(){
        if(running){
            return;
        }
        running = true;
        aiThread = new Thread(this);
        aiThread.start();

    }

    public synchronized void stop(){
        if(!running){
            return;
        }
        running = false;

        try {
            aiThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }




}
