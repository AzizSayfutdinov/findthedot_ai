package dev.Aziz.findTheDotAI.ai;

import dev.Aziz.findTheDotAI.Population.Population;
import dev.Aziz.findTheDotAI.Population.player.Player;

import java.awt.*;

public class World {

    Population population;

    public World(){
        population = new Population();
        population.addPlayer(new Player(150, 100));
    }

    public void update(){
        population.update();
    }

    public void render(Graphics g){

        g.setColor(Color.GREEN);
        g.fillRect(50,50,80, 20);
        population.render(g);

    }

}
