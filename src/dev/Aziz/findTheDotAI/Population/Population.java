package dev.Aziz.findTheDotAI.Population;

import dev.Aziz.findTheDotAI.Population.player.Player;

import java.awt.*;
import java.util.ArrayList;

public class Population {

    private ArrayList<Player> population;

    public Population(){
        population = new ArrayList<>();
    }

    public void addPlayer(Player player){
        population.add(player);
    }

    public void update(){
        for(Player p: population){
            p.update();
        }
    }

    public void render(Graphics g){
        for(Player p: population){
            p.render(g);
        }
    }

}
