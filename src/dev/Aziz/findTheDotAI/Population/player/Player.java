package dev.Aziz.findTheDotAI.Population.player;

import dev.Aziz.findTheDotAI.utils.Vector;

import java.awt.*;

public class Player {

    public static int PLAYER_WIDTH = 8;
    public static int PLAYER_HEIGHT = 8;

    // Debug
    private Vector directionToMove;

    //-----------

    private double x;
    private double y;

    public Player(double x, double y){
        this.x = x;
        this.y = y;
        directionToMove = new Vector(4,1);
    }

    public void update(){
        move();
    }

    public void render(Graphics g){
        g.setColor(Color.BLACK);
        g.fillOval((int)x, (int)y, PLAYER_WIDTH,PLAYER_HEIGHT);
    }

    private void move(){
        x = x + directionToMove.x;
        y = y + directionToMove.y;
    }

    public Vector getDirectionToMove() {
        return directionToMove;
    }

    public void setDirectionToMove(Vector directionToMove) {
        this.directionToMove = directionToMove;
    }

    public void setDirectionToMove(int x, int y){
        this.directionToMove.x = x;
        this.directionToMove.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
