package dev.Aziz.findTheDotAI;

import dev.Aziz.findTheDotAI.ai.AI;

public class Launcher {

    public static void main(String[] args){

        AI ai = new AI();
        ai.start();

    }

}
